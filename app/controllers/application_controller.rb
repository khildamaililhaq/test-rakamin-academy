# Default fucntion for HTTP Request
class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  before_action :current_user

  def current_user
    given_token = request.headers["Authorization"]

    decoded_cred = JsonWebToken.decode(given_token)

    @current_user ||= User.find_by(id: decoded_cred[:user_id])
  rescue StandardError
    render json: :unauthorized
  end
end
