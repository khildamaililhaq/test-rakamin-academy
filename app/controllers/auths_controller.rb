# All Function of Auths
class AuthsController < ApplicationController
  skip_before_action :current_user, only: [:login, :register]

  def login
    user = User.find_by(email: params[:email])

    if user.present?
      render json: user.authenticate(params[:password])
    else
      render json: { errors: "Incorrect Username" }, status: :unauthorized
    end
  rescue StandardError => e
    render json: { errors: e.message }, status: :unauthorized
  end

  def register
    user = User.create(user_params)

    if user.valid?
      token = JsonWebToken.encode({ user_id: user.id })
      render json: { token: token }
    else
      render json: { errors: user.errors }, status: :unprocessable_entity
    end
  end

  def me
    render json: @current_user
  end

  private

  def user_params
    params.permit(:email, :password, :name, :photo_url)
  end
end
