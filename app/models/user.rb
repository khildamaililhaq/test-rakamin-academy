class User < ApplicationRecord
  # encrypt password
  has_secure_password

  validates_presence_of :email, :password, :name
  validates_uniqueness_of :email

  # authenticate user by password
  def authenticate(password)
    raise StandardError, 'Incorrect Password' unless BCrypt::Password.new(password_digest) == password

    JsonWebToken.encode({ user_id: id })
  end
end
