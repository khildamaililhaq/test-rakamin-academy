Rails.application.routes.draw do
  namespace :auths do
    post 'register', to: 'register'
    post 'login', to: 'login'
    get 'me', to: 'me'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
